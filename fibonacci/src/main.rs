use std::env;

fn fib(n: u32) -> u32 {
    match n {
        0 => 0,
        1 => 1,
        _ => fib(n - 1) + fib(n - 2),
    }
}

fn main() {
    let argv: Vec<String> = env::args().collect();
    let n: u32 = argv[1].parse().expect("Error parsing input.");
    let result = fib(n);
    println!("{result}");
}
