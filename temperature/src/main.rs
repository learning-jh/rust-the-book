use std::io;

// Converts from Fahrenheit to Celsius.
fn f_to_c(t: f64) -> f64 {
    (t - 32.0) * (5.0 / 9.0)
}

// Converts from Celsius to Fahrenheit.
fn c_to_f(t: f64) -> f64 {
    t * (9.0 / 5.0) + 32.0
}

// Program converts temperatures between Celsius and Fahrenheit.
fn main() {
    loop {
        // Print program options.
        println!("Select option:");
        println!("1: Celsius convert to Fahrenheit");
        println!("2: Fahrenheit convert to Celsius");
        println!("3: Exit program");

        // Assign memory for option string.
        let mut option = String::new();

        // Attempt to read option input.
        io::stdin()
            .read_line(&mut option)
            .expect("Error when reading option input.");

        let option: u32 = match option.trim().parse() {
            Ok(opt) => opt,
            Err(_) => continue,
        };

        if option < 1 || option > 3 {
            println!("Invalid option.");
            continue;
        }

        if option == 3 {
            break;
        }

        // Print temperature input.
        println!("Input temperature:");

        // Assign memory for option string.
        let mut temp = String::new();

        // Attempt to read option input.
        io::stdin()
            .read_line(&mut temp)
            .expect("Error when reading option input.");

        let temp: f64 = match temp.trim().parse() {
            Ok(temp) => temp,
            Err(_) => continue,
        };

        let temp = match option {
            1 => c_to_f(temp),
            2 => f_to_c(temp),
            _ => continue,
        };
        println!("{temp}");
        break;
    }
}
