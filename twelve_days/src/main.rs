fn gift(day: u32) -> String {
    let gift = match day {
        1 => "a partridge in a pear tree.",
        2 => "Two turtle doves,",
        3 => "Three French hens,",
        4 => "Four calling birds,",
        5 => "Five gold rings,",
        6 => "Six geese a-laying,",
        7 => "Seven swans a-swimming,",
        8 => "Eight maids a-milking,",
        9 => "Nine ladies dancing,",
        10 => "Ten lords a-leaping,",
        11 => "Eleven pipers piping,",
        12 => "Twelve drummers drumming,",
        _ => "",
    };
    gift.to_string()
}

fn get_suffix(day: u32) -> String {
    let s = match day {
        1 => "st",
        2 => "nd",
        3 => "rd",
        _ => "th",
    };
    s.to_string()
}

fn main() {
    for day in 1..=12 {
        let suffix = get_suffix(day);
        println!("On the {day}{suffix} day of Christmas, my true love sent to me");
        for d in (1..=day).rev() {
            if d == 1 && day > 1 {
                print!("And ");
            };
            println!("{}", gift(d));
        }
    }
}
